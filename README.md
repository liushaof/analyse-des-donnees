# sy09-D1-groupe-5

Projet de SY09 (P21) - TD D1 - Groupe 5
# Data 
[Economic Guide to Picking a College Major](https://github.com/rfordatascience/tidytuesday/tree/master/data/2018/2018-10-16)
# Install

1. https://python-poetry.org/docs/
2. `poetry install`: install dependencies
3. `pre-commit install`: install pre-commit hooks

# Development workflow

1. Create `feature/...` branch
2. MR on `develop`
3. Create `release` branches and tags on milestones
4. Merge stable `release` into master

# Python formatter and static type checker : black and flake8

`black` is a python formatter. `flake8` is a static type checker.

# Rapport

```
├── rapport
│   ├── Biblio.bib
│   ├── Rapport.tex
│   ├── figures
│   │   ├── ...
│   └── sections
│       ├── 1_introduction.tex
│       ├── 2_analyses
│       │   ├── 0_analyse.tex
│       ├── 3_conclusion.tex
│       └── 4_annexes
│       │   ├── 0_annexe.tex
```

## Compilation

1. `pdflatex --shell-escape Rapport.tex`
2. `biber Rapport`
3. `pdflatex --shell-escape Rapport.tex`