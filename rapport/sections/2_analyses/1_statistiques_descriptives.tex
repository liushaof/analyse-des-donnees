% la partie principale, articulée suivant la problématique d'étude, présente les données et les analyses effectuées, de manière structurée;

% Dans le rapport :
% - reproduisez les grandes lignes du cheminement intellectuel qui vous a conduit aux résultats présentés. 
% - Les méthodes ou algorithmes utilisés doivent être décrits succinctement (sans pour autant sacrifier à la brièveté la précision et la clarté de vos explications).
% - Le code source devrait être évité dans le compte-rendu, à moins qu'ils ne présente un intérêt particulier ou ne soit absolument nécessaire à la compréhension (ce qui n'est généralement pas le cas). %
% - Nous conseillons de consigner les codes importants en annexe, pour ne pas perturber la lecture du document (et d'omettre les commandes décrivant des manipulations triviales).
% - L'analyse des résultats doit bien entendu dépasser les commentaires triviaux (tels que \og la méthode marche mal \fg{}, ou encore \og la méthode donne tel résultat \fg{}), qui trahissent un manque de réflexion et sont donc généralement sanctionnés.
% - Elle doit s'inscrire dans une démarche globale où l'on recherchera la cohérence : vos différentes contributions devront être articulées. 
% - Vous insisterez sur les aspects complémentaires des traitements que vous aurez mené (comme par exemple lorsqu'un test statistique vient compléter une représentation graphique).

\section{Présentation des données}
\subsection{Résumé}

Le jeu de données regroupe 173 spécialisations décrites au travers de 21 variables, dont 17 quantitatives qui apportent des informations selon 4 axes majeurs :
\begin{itemize}
    \item La population
    \item L'emploiement
    \item La typologie de l'emploi après le diplôme
    \item Les revenus
\end{itemize}

Les spécialisations sont réparties parmi 16 catégories :
\begin{multicols}{2}
    \begin{itemize}
        \small
        \item Engineering\normalfont{;}
        \item Business\normalfont{;}
        \item Physical Sciences\normalfont{;}
        \item Law \& Public Policy\normalfont{;}
        \item Computers \& Mathematics\normalfont{;}
        \item Agriculture \& Natural Resources\normalfont{;}
        \item Industrial Arts \& Consumer Services\normalfont{;}
        \item Arts\normalfont{;}
        \item Health\normalfont{;}
        \item Social Science\normalfont{;}
        \item Biology \& Life Science\normalfont{;}
        \item Education\normalfont{;}
        \item Humanities \& Liberal Arts\normalfont{;}
        \item Psychology \& Social Work\normalfont{;}
        \item Communications \& Journalism\normalfont{;}
        \item Interdisciplinary\normalfont{.}
    \end{itemize}
\end{multicols}

\subsection{Notations}
\begin{itemize}
    \item $X$, la matrice à $n=173$ lignes et $p=21$ colonnes.
    \item $x_i$ désigne le vecteur vecteur colonne associé à la $i$-ème spécialisation.
    \item $\xvar{TOTAL}$ désigne le vecteur colonne associé à la variable \texttt{TOTAL}.
    \item $\xind{TOTAL}{i}$ désigne la valeur de la variable \texttt{TOTAL} associée à la $i$-ème spécialisation
\end{itemize}

\subsubsection{Variables qualitatives}

Ce jeu de données propose 4 variables qualitatives :
\begin{itemize}
    \item  (\texttt{Rank}) Le rang est une variable ordinale obtenue en classant les spécialisations par ordre décroissant selon leur revenu annuel médian;
    \item (\texttt{Major\_code}) Un code administratif unique pour chaque spécialisation;
    \item  (\texttt{Major}) Le nom de la spécialisation : il est unique et informe sur le contenu de la spécialisation;
    \item (\texttt{Major\_category}) Une catégorie, qui provient d'un rapport du \textit{Center on Education and the Workforce, Georgetown University}~\cite{book:Carnevale_Strohl_Melton}.
\end{itemize}

\subsubsection{Variables quantitatives}\label{part:var_quantitatives}
L'interprétation des variables quantitatives a été établie à l'aide du script \texttt{R} original et du dictionnaire de codes de l'ACS PUMS 2010-2012~\cite{pdf:ACS_PUMS_DATA_DICTIONARY_2015}.

\paragraph{Population}
Sommer les pondérations associées aux individus dans le PUMS permet d'estimer la taille d'une population.
Dans l'ACS, le sexe de l'individu est codé selon 2 modalités :
\begin{enumerate}
    \item[1] \textit{Male};
    \item[2] \textit{Female}.
\end{enumerate}

\subparagraph{\texttt{Total}}\label{var:Total}
Estimation de la taille de la population totale qui a suivi la spécialisation.
\subparagraph{\texttt{Men}}\label{var:Men}
Estimation de la taille de la population d'hommes qui a suivi la spécialisation.
\subparagraph{\texttt{Women}}\label{var:Women}
Estimation de la taille de la population de femmes qui a suivi la spécialisation.
\subparagraph{\texttt{ShareWomen}}\label{var:ShareWomen}
Part de femmes parmi la population totale.
\begin{equation}\label{eq:sharewomen}
    \texttt{ShareWomen} = \frac{\texttt{Women}}{\texttt{Women}+\texttt{Men}}
\end{equation}

La variable \texttt{Total} est une combinaison linéaire de \texttt{Men} et \texttt{Women}, ce résultat est vérifié sur le jeu de données.

\begin{equation}\label{eq:total_eq_men_plus_women}
    \texttt{Total}=\texttt{Men}+\texttt{Women}
\end{equation}

\paragraph{Emploiement}
Dans l'ACS, le statut d'emploiement de l'individu est codé parmi 7 catégories :
\begin{enumerate}
    \item[b] N/A \textit{(less than 16 years old)};
    \item[1] \textit{Civilian employed, at work};
    \item[2] \textit{Civilian employed, with a job but not at work};
    \item[3] \textit{Unemployed};
    \item[4] \textit{Armed forces, at work};
    \item[5] \textit{Armed forces, with a job but not at work};
    \item[6] \textit{Not in labor force}.
\end{enumerate}

\subparagraph{\texttt{Employed}}\label{var:Employed}
Estimation de la taille de la population d'employés (catégorie 1 ou 2).
\subparagraph{\texttt{Unemployed}}\label{var:Unemployed}
Estimation de la taille de la population de chômeurs (catégorie 3).
\subparagraph{\texttt{Unemployment\_rate}}\label{var:Unemployment_rate}
Part de chômeurs parmi la population d'employés.

\begin{equation}\label{eq:unemployment_rate}
    \texttt{Unemployment\_rate} = \frac{\texttt{Unemployed}}{\texttt{Unemployed}+\texttt{Employed}}
\end{equation}

De façon contre-intuitive, la somme d'\texttt{Employed} et d'\texttt{Unemployed} n'est pas égale au \texttt{Total}. En effet, selon le codage utilisé par l'ACS, les forces armées et la population inactive (ni employé, ni au chômage~\cite{site:not_in_labor_force}) ne sont pas comptabilisées dans la population active.

\paragraph{Typologie de l'emploi}
La typologie proposée est parfois confuse, car le référentiel choisi n'est pas identique pour toutes les variables.

\subparagraph{\texttt{Full\_time}}\label{var:Full_time}
Estimation de la taille de population d'employés à plein temps selon le filtre suivant : temps travaillé supérieur à 35h par semaine les 12 derniers mois.

\subparagraph{\texttt{Part\_time}}\label{var:Part_time}
Estimation de la taille de population d'employés à temps partiel selon le filtre suivant : temps travaillé strictement inférieur à 35h par semaine les 12 derniers mois.

À noter qu'aucun filtre sur le statut d'emploiement n'est réalisé ici. La somme de \texttt{Full\_time} et \texttt{Part\_time} est donc \textit{supérieure} à \texttt{Employed} (sur lequel s'applique les filtres 1 et 2) dans le jeu de données.

\subparagraph{\texttt{Full\_time\_year\_round}}\label{var:Full_time_year_round}
Estimation de la taille de population d'employés à temps plein tout au long de l'année selon les critères suivants :
\begin{itemize}
    \item temps travaillé supérieur à 35h par semaine les 12 derniers mois;
    \item avoir travaillé entre 50 et 52 semaines les 12 derniers mois.
\end{itemize}

\subparagraph{\texttt{College\_jobs}}\label{var:College_jobs}
Estimation de la taille de population d'employés dont la profession exige un diplôme universitaire (la liste est privée) selon les filtres suivants :
\begin{itemize}
    \item statut d'emploiement de catégorie 1 ou 2;
    \item la profession est qualifiée comme exigeant un diplôme universitaire.
\end{itemize}

\subparagraph{\texttt{Non\_college\_jobs}}\label{var:Non_college_jobs}
Estimation de la taille de population d'employés dont la profession n'exige un diplôme universitaire (même liste que \texttt{College\_jobs}) selon les filtres suivants :
\begin{itemize}
    \item statut d'emploiement de catégorie 1 ou 2;
    \item la profession est qualifiée comme n'exigeant pas de diplôme universitaire.
\end{itemize}

Contrairement aux variables \texttt{Full\_time}, \texttt{Part\_time} et \texttt{Full\_time\_year\_round}, les quantités \texttt{College\_jobs} et \texttt{Non\_college\_jobs} prennent les mêmes filtres que la population active (\texttt{Employed}). Néanmoins, leur somme n'est pas égale à \texttt{Employed}. La liste utilisée pour qualifier la profession n'est donc pas complète.

\subparagraph{\texttt{Low\_wage\_jobs}}\label{var:Low_wage_jobs}
Estimation de la taille de population d'employés dont la profession est peu rémunérée (liste publique disponible sur~\cite{article:Casselman_2014}) selon le filtre suivant : la profession est qualifiée comme peu rémunérée.

Aucun filtre n'est réalisé sur le statut d'emploiement, ni sur temps de travail. Également, comme la liste des professions exigeant (ou inversement) un diplôme universitaire n'est pas connue, la possibilité d'une intersection non vide entre les deux listes est probable. Quand bien même l'intersection serait vide, la somme des quantités \texttt{College\_jobs}, \texttt{Non\_college\_jobs} et \texttt{Low\_wage\_jobs} n'est pas égale à la quantité \texttt{Employed}. L'union des deux listes ne contient pas la liste complète des professions. Comme expliqué en~\ref{expl:military_tech}, les professions susceptibles d'être absentes de la liste relèvent du parcours militaire.

\paragraph{Revenus}
Les statistiques de revenus sont établies sur un sous-ensemble filtré différemment que les statistiques de population, notamment les diplômés ayant obtenu un autre diplôme que la licence universitaire ou ayant continué en thèse ne sont pas comptabilisés. Également, l'inflation n'est pas prise en compte.

\subparagraph{\texttt{Sample\_size}}\label{var:Sample_size}
Quantité d'individus pour estimer les statistiques de revenus annuels, à condition :
\begin{itemize}
    \item d'avoir un revenu annuel strictement positif;
    \item d'avoir travaillé 50 et 52 semaines les 12 derniers mois;
    \item d'avoir travaillé plus de 35h par semaine les 12 derniers mois;
    \item de ne pas avoir fréquenté d'école dans les trois derniers mois;
    \item d'avoir obtenu un diplôme équivalent à une licence universitaire (\textit{Bachelor's degree}).
\end{itemize}

\subparagraph{\texttt{Median}}\label{var:Median}
Estimation du revenu annuel médian.

\subparagraph{\texttt{P25th}}\label{var:P25th}
Estimation du premier quartile du revenu annuel.

\subparagraph{\texttt{P75th}}\label{var:P75th}
Estimation du troisième quartile du revenu annuel.

\subsubsection{Valeurs manquantes, aberrantes}

Le jeu de données ne nécessite pas de pré-traitement particulier.
Seule la spécialisation \textit{Food science} souffre de valeurs manquantes sur les variables de population. Nous préfèrerons enlever cette spécialisation lorsqu'il est nécessaire de le faire plutôt que d'en approximer les valeurs manquantes. Au vu du peu de spécialisations dans la catégorie associée (\textit{Agriculture \& Natural Resources}), remplacer les valeurs manquantes par une estimation laisserait place à une trop grande erreur. De plus, la population d'une spécialisation à une autre au sein même d'une catégorie peut varier grandement.

\subsection{Exploration élémentaire des données}
Une exploration préalable des données est proposée au cours de laquelle les caractéristiques les plus importantes du jeu de données seront mises en valeurs.

\subsubsection{Aperçu général des spécialisations}

Par la suite, les spécialisations seront fréquemment regroupées pour observer certaines tendances. Le jeu de données propose un groupement évident par catégorie de spécialisation (\texttt{Major\_category}).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/nb_spe_by_category.pdf}
    \caption{Nombre de spécialisations par catégorie}
    \label{fig:nb_spe_by_category}
\end{figure}

Nous observons sur la figure (\ref{fig:nb_spe_by_category}) que la catégorie \textit{Engineering} comporte le plus de spécialisations (29 au total). Les autres catégories en comporte une dizaine en moyenne. Des différences au niveau de la fréquentation existent également. En effet, là où \textit{Psychology} totalise presque 400 000 diplômés, \textit{Military technologies} n'en compte que 124. La répartition par catégorie est observable en figure \ref{fig:cat_total}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/category_total.pdf}
    \caption{Nombre de diplômés par catégorie}
    \label{fig:cat_total}
\end{figure}

\subsubsection{Aperçu général des populations}

La population est constitué à 57,5\% de femmes, et 42,5\% d'hommes, avec une prédominance des hommes dans les catégories de STEM\footnote{\textit{Science, technology, engineering, and mathematics}} (\textit{Engineering}, \textit{Computers \& Mathematics}, \dots), et une prédominance des femmes dans les catégories de sciences humaines (\textit{Education}, \textit{Health}, \textit{Humanities \& Liberal Arts}, \textit{Psychology \& Social Work}, \dots).

La répartition de la population en figure~\ref{fig:pop_by_major_category_by_sex} par catégorie montre qu'une majorité d'étudiants choississent des spécialisations liées aux affaires et que la répartition par sexe n'est pas uniforme selon les catégories.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/pop_by_major_category_by_sex.pdf}
    \caption{Population par catégorie et par sexe}
    \label{fig:pop_by_major_category_by_sex}
\end{figure}

\subsubsection{Aperçu général de la typologie des emplois}

Parmi les 5 spécialisations avec le taux d'emploi peu rémunéré le plus fort, 3 appartiennent à la catégorie \textit{Arts}. C'est la catégorie d'emplois avec une situation financière la moins avantageuse, suivie de \textit{Humanities \& Liberal Arts} et \textit{Communications \& Journalism}. Les 3 catégories souffrant en revanche le moins d'emplois peu rémunérés sont : \textit{Engineering}, \textit{Computer \& Mathematics} et \textit{Health}.

Enfin, la spécialisation \textit{Nuclear enginnering} est celle présentant le plus haut taux de chômage (presque 18\%) alors que d'autres comme (\textit{Botany}, \textit{Military technology} sont à 0\%. Cependant, les taux de chômage ne paraissent pas corrélés à la catégorie de spécialisation et semblent intrinsèques aux spécialisations directement.


\subsubsection{Aperçu général des revenus annnuels}

En figure~\ref{fig:histplot_median} est visualisé la distribution des revenus médians annuels (les allures de \texttt{P25th} et \texttt{P75th} sont similaires). Cette distribution ne suit pas une loi normale, et ce pour toutes les statistiques de revenu. Le test de Shapiro-Wilk permet de rejeter l'hypothèse de normalité au niveau de signification 99,9\%.

Aussi, nous observons la présence d'une valeur extrême au délà des 100 000 dollars annuel. Il s'agit de la spécialisation \textit{Petroleum engineering}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/histplot_median.pdf}
    \caption{Distribution des revenus médians annuels}
    \label{fig:histplot_median}
\end{figure}


\subsubsection{Valeurs extrêmes}

Quelques statistiques sont présentées en page \pageref{table:statistiques_revenus}. Comme précisé dans l'article de presse~\cite{article:Casselman_2014}, la taille de l'échantillon pour les statistiques de revenu est parfois très petite (2 individus), et 25\% des spécialisations présentent moins de 39 individus. Les statistiques de revenus sont donc parfois très peu fiables.

\label{expl:military_tech}
Aussi, toutes les statistiques hormis celles en pourcentages comportent de fortes valeurs extrêmes des deux côtés, parfois dû aux filtres réalisés dans la construction du jeu de données. La spécialisation \textit{Military Technologies} est de façon évidente victime du mauvais traitement des données : $111$ individus travaillent à plein temps toute l'année, mais $0$ individu sont employés de catégorie 1 ou 2; aucun individu n'a vu sa profession être qualifiée, ce qui laisse suggérer que la liste des professions ne contient pas celles provenant du domaine militaire.

\subsubsection{Corrélations}

Le coefficient de corrélation de Spearman est utilisé, car plus robuste aux valeurs extrêmes~\cite{paper:Khamis_2008}.

Les variables de population sont corrélés positivement entre elles, car toutes combinaisons linéaires entre elles à une constante près (car les filtres ne sont pas réalisés de façon identique pour toutes les variables).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/var_spearman_corr.pdf}
    \caption{Carte de chaleur des corrélations des variables quantitatives}
    \label{fig:spearman_corr}
\end{figure}

Aucune association ne semble exister entre le taux de chômage, et les autres variables. La quantité d'emplois peu rémunérés est légèrement anti-corrélée aux variables de revenu médian, ce qui peut s'expliquer simplement par les revenus annuels inférieurs faisant diminuer les quantiles.

En revanche, la proportion de femmes (\texttt{ShareWomen}) semble fortement anti-corrélée aux statistiques de revenu annuel. En effet, le coefficient de corrélation de Spearman entre la part de femmes et le revenu annuel médian est de $-0,66$.