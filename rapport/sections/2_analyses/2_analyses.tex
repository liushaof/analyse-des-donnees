% la partie principale, articulée suivant la problématique d'étude, présente les données et les analyses effectuées, de manière structurée;

% Dans le rapport :
% - reproduisez les grandes lignes du cheminement intellectuel qui vous a conduit aux résultats présentés. 
% - Les méthodes ou algorithmes utilisés doivent être décrits succinctement (sans pour autant sacrifier à la brièveté la précision et la clarté de vos explications).
% - Le code source devrait être évité dans le compte-rendu, à moins qu'ils ne présente un intérêt particulier ou ne soit absolument nécessaire à la compréhension (ce qui n'est généralement pas le cas). %
% - Nous conseillons de consigner les codes importants en annexe, pour ne pas perturber la lecture du document (et d'omettre les commandes décrivant des manipulations triviales).
% - L'analyse des résultats doit bien entendu dépasser les commentaires triviaux (tels que \og la méthode marche mal \fg{}, ou encore \og la méthode donne tel résultat \fg{}), qui trahissent un manque de réflexion et sont donc généralement sanctionnés.
% - Elle doit s'inscrire dans une démarche globale où l'on recherchera la cohérence : vos différentes contributions devront être articulées. 
% - Vous insisterez sur les aspects complémentaires des traitements que vous aurez mené (comme par exemple lorsqu'un test statistique vient compléter une représentation graphique).

\section{Analyses}

\subsection{Analyse en composantes principales}
% ACP

\subsubsection{Motivations}
Afin de rendre ce jeu de données utile et de répondre à la problématique mentionnée en introduction, nous proposons ici de réaliser une analyse en composantes principales. L'objectif n'est pas uniquement de réduire les dimensions de notre espace, mais également d'observer la relation des variables entre elles. La visualisation des individus nous sert à identifier des groupes de spécialisations.

\subsubsection{Sélection des variables}
% TODO : expliquer pourquoi on sélectionne ces variables et pas les autres.
Au cours de cette étude, nous privilégierons le salaire médian au rang, dans l'objectif d'éviter la perte d'information liée à l'échelle ordinale. Le retrait des variables qualitatives et de celles non relevantes par rapport aux individus considéré nous conduit à garder les variables suivantes.

\begin{multicols}{2}
    \begin{enumerate}
        \item \texttt{Total};
        \item \texttt{Men};
        \item \texttt{Women};
        \item \texttt{Employed};
        \item \texttt{Unemployed};
        \item \texttt{Full\_time};
        \item \texttt{Part\_time};
        \item \texttt{Full\_time\_year\_round};
        \item \texttt{College\_jobs};
        \item \texttt{Non\_college\_jobs};
        \item \texttt{Low\_wage\_jobs};
        \item \texttt{Median};
        \item \texttt{P25th};
        \item \texttt{P75th}.
    \end{enumerate}
\end{multicols}

\subsubsection{Pré-traitement des données}\label{part:acp}
Avant de réaliser une ACP sur le jeu de données, il convient de s'intéresser aux variables et de vérifier certaines hypothèses. L'analyse exploratoire nous montre que :
\begin{enumerate}
    \item Certaines variables subissent un effet taille;\label{enum:effet_taille}
    \item La notion de norme euclidienne n'a ici pas de sens direct dans l'espace initial considéré.\label{enum:norme_euclidienne}
\end{enumerate}

Concernant le premier point~(\ref{enum:effet_taille}), les variables 1 à 10 se rapportent directement à la population totale de diplômés. Pour contrer cette effet, les valeurs concernées sont normalisées par le \texttt{Total} :

\begin{equation}\label{eq:var_par_total}
    \overset{\sim}{\xind{j}{i}} = \frac{\xind{j}{i}}{\xind{Total}{i}},\qquad j=1,\ldots,10
\end{equation}

Quant au second point~(\ref{enum:norme_euclidienne}), les grandeurs sont mesurées avec des échelles différentes. Les variables de revenu annuel sont de l'ordre de $10^2$. Pour ramener les variables dans un espace euclidien centré, les variables sont standardisées.

\begin{equation}\label{eq:variables_standardisation}
    \overset{\sim}{\xind{j}{i}} = \frac{\xind{j}{i} - \bar{x}^j}{s^j}
\end{equation}

$\bar{x}^j$ et $s^j$ désignent respectivement la moyenne empirique et l'écart-type empirique de la $j$-ème variable.

\subsubsection{Réalisation de l'ACP}

Les variables sélectionnées et traitées, nous procédons à l'ACP. Les pourcentages d'inertie expliqués peuvent être observés en figure \ref{fig:acp_inertia}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/acp_inertie.pdf}
    \caption{Inertie des axes factoriels en pourcentage}
    \label{fig:acp_inertia}
\end{figure}

L'application de la méthode du coude pour le nombre de composantes à retenir nous oriente vers le choix des 2 à 3 premières. Étant donnée la proportion d'inertie non négligeable expliquée par la troisième composante (12,87\%) nous la conserverons afin de ne pas trop perdre d'information sur notre nuage de points initial. Ainsi, l'inertie expliqué par les 3 axes retenus s'élève à 67,82\%.

\subsubsection{Cercle de corrélation des variables}

La représentation des corrélations des variables sur deux axes factoriels permet non seulement de visualiser la relation entre les variables, mais également d'interpréter les nouveaux axes, sans négliger la qualité de projection des variables sur les axes.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/variable_factor_map_1_2.pdf}
    \caption{Cercle de corrélation pour les axes factoriels 1 et 2}
    \label{fig:variable_factor_map_1_2}
\end{figure}

La figure~\ref{fig:variable_factor_map_1_2} met en évidence cette projection des variables dans le premier plan factoriel. Sur les variables correctement représentées (\textit{i.e.} avec un \texttt{cos2} suffisamment grand) et excluant les relations évidentes par construction, une anti-corrélation forte est observée entre la quantité d'emploi peu rémunéré et les statistiques de revenu. Cette observation confirme une intuition assez basique. Plus le nombre d'emplois sous-payés est élevé, plus les statistiques de salaires sont basses.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/variable_factor_map_1_3.pdf}
    \caption{Cercle de corrélation pour les axes factoriels 1 et 3}
    \label{fig:variable_factor_map_1_3}
\end{figure}

Sur les axes 1 et 3 en figure~\ref{fig:variable_factor_map_1_3}, une anti-corrélation forte est observée entre la proportion de femmes et les statistiques de revenu.

Concernant l'interprétation des axes factoriels, le premier axe est très corrélé aux populations d'hommes, ainsi qu'aux statistiques de revenus annuels et fortement anti-corrélés aux populations de femmes et d'emploi peu rémunéré ou à temps partiel. Quant aux autres variables à l'exception de \texttt{Employed}, \texttt{Total}, d'\texttt{Unemployed} (corrélés respectivement aux axes factoriels 4, 5, et 6), l'interprétation en est difficile car elles sont corrélés aux trois premiers axes à la fois. On peut cependant considérer l'hypothèse que le deuxième axe factoriel corresponde aux statistiques d'emploiement à temps plein. Les emplois exigeant un diplôme universitaire sont corrélés au troisième axe factoriel.

\subsubsection{Visualisation des spécialisations}

Nous pouvons projeter nos individus selons les axes factoriels 1, 2, et 3 en figure~\ref{fig:individual_factor_map_1_2}. L'ajout d'une coloration et de symboles par catégorie de spécialisation permet de mieux visualiser et rendre compte de la répartition des spécialisations.

\begin{figure*}
    \begin{subfigure}[c]{0.39\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/individual_factor_map_1_2.pdf}
        \caption{Représentation des spécialisations pour les axes factoriels 1 et 2}
        \label{fig:individual_factor_map_1_2}
    \end{subfigure}
    \hfill%
    \begin{subfigure}[c]{0.39\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/individual_factor_map_1_3.pdf}
        \caption{Représentation des spécialisations pour les axes factoriels 1 et 3}
        \label{fig:individual_factor_map_1_3}
    \end{subfigure}
    \begin{subfigure}[c]{0.19\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/individual_factor_map_legend.png}
        \caption{Légende}
        \label{fig:individual_factor_map_legend}
    \end{subfigure}
    \caption{Représentation des spécialisations selon les trois premiers axes factoriels}
\end{figure*}

Sans analyse préalable sur la participation des v>ariables aux axes, nous pouvons noter que le nuage d'individus est assez diffus, mais que sa répartition n'est pas uniforme. Les spécialisations sont plus denses dans les 2ème et 3ème quadrants, expliqué par la proportion plus importante de femmes dans les populations étudiées.

Aussi, nous pouvons observer dans le quatrième quadrant une prédominance des spécialisations attenantes à l'ingénierie, qui combinent donc les caractéristiques suivantes : revenu annuel médian élevé, prédominance des hommes, et comme les spécialisations semblent dispersés selon le deuxième axe factoriel, également une forte variabilité parmi l'emploi à temps plein.

Une analyse similaire peut être effectué pour les spécialisations d'\textit{Agriculture \& Natural Resources} et \textit{Business}, qui ont une plus forte proportion d'emplois à temps plein. Ou encore, les catégories \textit{Arts}, \textit{Humanities \& Liberal Arts} qui ont une plus forte proportion de femmes. Enfin, les spécialisations d'\texttt{Education} aboutissent plus souvent à un emploi exigeant un diplôme universitaire.

En somme, ces deux projections sont riches en informations. Via cette ACP, la compréhension du jeu de données est bien plus aisée et surtout elle pourrait permettre d'aider au choix d'une spécialisation. Par exemple, un étudiant souhaitant s'orienter vers des spécialisations du domaine de la santé devra s'attendra à étudier dans un milieu à majorité féminine, avec une forte proportion d'emploi à temps partiel à la première embauche. Cependant, il convient aussi de s'assurer de la qualité de projection des individus dans les trois premiers axes factoriels.

\subsubsection{Qualité de projection des individus}

En figure~\ref{fig:individual_cos2_1_2_3_sum}, la qualité de projection des spécialisations sur chacun des axes factoriels est très dispersée, mais leur somme montre que pour la majorité des individus, la projection sur les trois premiers axes factoriels n'induit pas en erreur.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/cos2_individual_1_2_3_sum.pdf}
    \caption{Qualité de projection des individus selon les 3 premiers axes factoriels et leur somme}
    \label{fig:individual_cos2_1_2_3_sum}
\end{figure}

\subsubsection{Synthèse}

L'ACP réalisée ici facilite l'interprétation des données. Nous y voyons de forts clivages entre les spécialisations menant à une situation financière stable (rémunératrices et avec un emploi à temps plein) et celles offrant des conditions plus précaires (avec un salaire moindre et une situation d'emploi instable). Il est aussi intéressant de constater que ces caractéristiques suivent la répartition homme/femme des spécialisations.

\subsection{Clustering}

\subsubsection{Motivations}

Nous avons vu lors de l'analyse exploratoire que le jeu de données présente un agrégat naturel par catégorie de spécialisation. Qui plus est, via l'ACP réalisée précédemment et les projections des individus effectuées certains groupes semblent se dessiner. Par exemple, la catégorie \texttt{Engineering} tend à se démarquer sur la partie droite des représentations.
Nous proposons ici d'évaluer la pertinence de ce groupement au sens des variables considérées. En effet, si le nom de la catégorie peut être facilement inféré à partir du nom de la spécialisation, nous cherchons à savoir si les descriptions quantitatives de chaque spécialisation permettent de former les mêmes groupements.

\subsubsection{Pré-traitement}
Les variables quantitatives ici utilisées correspondent à celles pré-traitées lors de l'ACP en partie \ref{part:acp}.

\subsubsection{Application des k-means}

Pour étudier cet aspect, nous proposons un protocole simple. Dans un premier temps, nous effectuons un clustering via la méthode des k-means en prenant comme nombre de classes le nombre de catégorie de spécialisation. L'initialisation des centres est réalisée par l'algorithme k-means++. Il est alors possible de calculer l'indice de Rand ajusté entre le résultat des k-means et les partitions pré-établies dans le jeu de données. Celui-ci s'élève à une valeur moyenne de 0,229 pour 100 répétitions.

Au vu de cette valeur, nous pouvons déduire que le partitionnement obtenu par la méthode des k-means et celui fourni par les catégories de spécialisation sont très éloignés.

Nous proposons alors d'étudier l'influence du nombre de clusters sur l'indice de Rand ajusté, afin de déterminer si un nombre plus faible de cluster pourrait proposer une répartition plus proche de celle des catégories de spécialisations.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/rand_cluster_nb.pdf}
    \caption{Évolution de l'indice de Rand en fonction du nombre de clusters}
    \label{fig:adj_rand_cluster_nb}
\end{figure}

La figure \ref{fig:adj_rand_cluster_nb} montre que la proximité entre les deux partitionnements reste globalement très faible (inférieure à 30\%), et ce pour toutes les valeurs du nombre de clusters.

Au vu des faibles correspondances des partitions obtenus par k-means vis à vis du groupement naturel par catégorie de spécialisation, nous nous intéressons aux partitionnements obtenus pour caractériser les groupements types lors de l'application d'un k-means à 16 clusters. Pour cela, nous répétons le clustering 100 fois et comparons les partitions obtenues avec l'indice de Rand ajusté via une AFTD. La dissimilarité considérée est alors $1-Ajusted Rand Index$.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/aftd_kmeans.pdf}
    \caption{Visualisation des différentes partitions après AFTD}
    \label{fig:aftd_kmeans}
\end{figure}

Le nuage de point obtenu en figure \ref{fig:aftd_kmeans} montre que les partitionnements sont tous très différents. Aucun groupe typique ne semble se dégager et nous pouvons en conclure que le clustering par la méthode des k-means produit un résultat directement dépendant de l'initialisation.

\subsubsection{Approfondissement}

Les observations formulées dans la partie précédente suggèrent que le partitionnement par k-means ne parvient pas à regrouper les spécialisations par catégorie, telles que définies dans le jeu de données. Nous proposons ici un aperçu plus en détails des clusters obtenus. En effet, nous cherchons à voir si malgré les faibles valeurs de l'indice de Rand ajusté obtenu, certaines catégories de spécialisations se retrouvent groupées dans un cluster, et si à l'inverse certains clusters ne contiennent qu'une catégorie de spécialisation particulière.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/elem_by_cluster.pdf}
    \caption{Répartition des éléments de 4 classes en clusters}
    \label{fig:elem_by_cluster}
\end{figure}

Le graphique \ref{fig:elem_by_cluster} montre pour 4 catégories la répartition des spécialisations au sein des différents clusters. Notons que pour alléger la lecture, nous avons ici retenu 4 exemples significatifs, qui illustrent au mieux le jeu de données et avons retiré les valeurs nulles. Notons également qu'il n'y a aucune indication d'appartenance à des clusters particuliers. Nous étudions ici, pour chaque catégorie, la décroissance du nombre d'éléments par cluster. Ainsi, nous pouvons voir que certaines catégories semblent regroupées au sein d'un nombre restreint de clusters. Les \textit{Arts} et l'\textit{Éducation} en sont l'exemple. À l'inverse, d'autres catégories sont disséminées de manière plus ou moins uniforme au sein de différents clusters.

D'un autre côté, nous étudions la pertinence des clusters au sens des catégories de spécialisation. Si dans le premier cas les \textit{Arts} semblent regroupés ensemble, rien ne garantit que le cluster ne contienne que cette catégorie.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/category_by_cluster.pdf}
    \caption{Constitution relative de 4 clusters}
    \label{fig:category_by_cluster}
\end{figure}


Effectivement, selon le graphique \ref{fig:category_by_cluster}, la composition des clusters semble très hétérogène. Le graphique est restreint à 4 clusters exemples pour ne pas nuire à la lisibilité.

En somme, ces observations mettent en évidence la difficulté de former un partitionnement fidèle aux classes pré-établies par les catégories de spécialisation.

\subsubsection{Synthèse}

Nous avons vu via l'algorithme des k-means que les catégories de spécialisation ne sont pas directement explicables au travers des variables considérées ici. Qui plus est, nous avons aussi pu remarquer que les partitions réalisées sont très différentes entre elles et dépendent directement de l'initialisation de l'algorithme.

Enfin, les différentes spécialisations se retrouvent très souvent mélangées dans différentes partitions. En ce sens, si nous souhaitons utiliser ce jeu de données pour recommander certaines spécialisations à des étudiants (en fonction de critères établis de leur côté), il convient d'adopter une granularité assez fine. En effet, émettre une recommandation telle que \textquote{aller dans les métiers de l'ingénierie} pourrait conduire à choisir une spécialisation dans laquelle les critères voulus ne sont pas respectés.
Ce dernier point nous permet de souligner finalement que les projections réalisées via l'ACP sont à considérer avec précautions. Les groupes qui semblent se discerner immédiatement le sont dans les plans factoriels considérés, mais cela perd de sa signification en considérant 100\% de l'information disponible dans le jeu de données.
